import logging

import flask_login
from flask import Flask, render_template, current_app, request
from flask_jwt_extended import JWTManager
from flask_migrate import Migrate
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

from config import app_config

# Importation des paramètres du fichier config

# Instantiation de la BD
db = SQLAlchemy()

# Instantiation du module API
api = Api()

# Instantiation du module JSON Web Token Authorization
jwt = JWTManager()

# Initialisation de loginManager pour la gestion de l'authentification
login_manager = flask_login.LoginManager()

from app import models, resources

api.add_resource(resources.UserRegistration, '/chatbots/api/registration')
api.add_resource(resources.UserLogin, '/chatbots/api/login')
api.add_resource(resources.AllUsers, '/chatbots/api/users')
api.add_resource(resources.GetbotReponse, '/chatbots/api/reponse')


def create_app(config_name):
    # Configuration pour l'importation des paramètres des fichiers configs
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config.from_pyfile('config.py')

    # Initialisation module bd
    db.init_app(app)

    # LoginManager configurations
    login_manager.init_app(app)
    login_manager.login_message = "Vous devez vous authentifier pour accéder à cette partie de l'application"
    login_manager.login_view = "auth.login"

    # Importation dans la BD des modifications faites sur les classes
    migrate = Migrate(app, db)

    # initialisation module API
    api.init_app(app)

    # initialisation de jwt
    jwt.init_app(app)

    handler = logging.FileHandler(app.config['LOGGING_LOCATION'])
    handler.setLevel(app.config['LOGGING_LEVEL'])
    formatter = logging.Formatter(app.config['LOGGING_FORMAT'])
    handler.setFormatter(formatter)

    # Initialisation des blueprints de chaque partie de l'appli
    from .admin import admin as admin_blueprint
    app.register_blueprint(admin_blueprint, url_prefix='/admin')

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .home import home as home_blueprint
    app.register_blueprint(home_blueprint)
    app.logger.addHandler(handler)

    app.register_error_handler(404, page_not_found)

    return app


def page_not_found(error):
    current_app.logger.error('Page not found: %s %s', request.path, error)
    return render_template('error/404.html'), 404
