# -*- coding:utf8 -*-
# !/usr/bin/env python
# Copyright 2017 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import uuid


import apiai

from flask import json


#CLIENT_ACCESS_TOKEN = '75223831e6574b68a59dd0763257b9a5'

#Recuperer le reponse json du dialogflow

def ReponseBotDialog(userMessage, CLIENT_ACCESS_TOKEN):
    ai = apiai.ApiAI(CLIENT_ACCESS_TOKEN)

    request = ai.text_request()

    request.lang = 'fr'  # optional, default value equal 'en'

    request.session_id = uuid.uuid4().hex

    request.query = userMessage

    response = request.getresponse()

    datastore = json.loads(response.read())



    return  datastore

#print (ReponseBotDialog("information sur ce numero",CLIENT_ACCESS_TOKEN))