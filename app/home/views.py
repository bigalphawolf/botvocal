from datetime import date

import werkzeug
from flask import render_template, request, jsonify, flash, app, current_app
from flask_login import login_required, current_user

from app import db
from ..models import Ticket
from ..botVocal import bot_home, bot_om, bot_re, recordSend
from . import home


@home.route('/dashboard')
@login_required
def dashboard():
    if current_user.is_authenticated:
        return render_template('home/botAccueil.html')
    return render_template('auth/login.html')


@home.route("/get", methods=['GET', 'POST'])
def get_bot_response():
    # Recupération du message de l'user
    usertxt = request.args.get('msg')

    # Recuperation de l'id du bot auquel on est connecté
    bot = request.args.get('bot')

    isformOM = False
    isformRE = False

    if bot == "botHOME":
        idbotG, reponse = bot_home(usertxt)
    elif bot == "botOM":
        idbotG, isformOM, reponse = bot_om(usertxt)
    else:
        idbotG, isformRE, reponse = bot_re(usertxt)

    if isformRE:
        return render_template(reponse)
    elif isformOM:
        return reponse
    else:
        return jsonify({'reponse': reponse, 'bot': idbotG})


@home.route('/reclam', methods=['POST'])
def reclam():
    # Recupération données utilisateur
    nameClient = request.form["nom"]
    prenomClient = request.form["prenom"]
    numeroClient = request.form.get("numero")
    derangement = request.form["derangement"]
    commentaire = request.form["commentaire"]

    today = date.today()
    istoday = str(today.day) + "/" + str(today.month) + "/" + str(today.year)
    ticket = Ticket(nameClient, prenomClient, numeroClient, derangement, commentaire, istoday, current_user.id)
    # Enregistrement des données dans la bd
    try:
        ticket.enregistrer()

        return jsonify(
            {'success': "ok"})
    except:
        return jsonify({'erreur': "Erreur lors de l'enregistrement"})


#
# @home.route("/getVocal")
# def get_bot_responseVocal():
#     # msg = recordSend()
#     msg = request.args.get('msg')
#     idbotG = request.args.get('bot')
#
#     isformOM = False
#     isformRecla = False
#
#     # voix.stopthread()
#     if idbotG == "botHOME":
#         idbotG, reponse = bot_home(msg)
#     elif idbotG == "botOM":
#         idbotG, isformOM, reponse = bot_om(msg)
#     else:
#         idbotG, isformRecla, reponse = bot_re(msg)
#
#     # data = msg + ";" + reponse
#     data = reponse
#     if isformOM == False and isformRecla == False:
#         # speak(reponse)
#         print("ok")
#     elif isformRecla:
#         # return msg + ";" + render_template(reponse)
#         return  render_template(reponse)
#     # return redirect(url_for(form))
#     return data
