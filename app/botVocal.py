import pymysql
import speech_recognition as sr
from sqlalchemy.orm import scoped_session, sessionmaker

from .models import Ticket
from app.send_msg_dialogFlow import ReponseBotDialog

db_session = scoped_session(sessionmaker(autocommit=False,autoflush=False))


#################################################
## traduire le text en parole ##
################################################
# def speak(audioString):
#     print(audioString)
#     tts = gTTS(text=audioString, lang='fr')
#     tts.save("audio.mp3")
#     os.system("mpg321 audio.mp3")


####################################################
## Reconnaissance de la parole (Speech recognition)#
###################################################
def recordSend():
    r = sr.Recognizer()
    with sr.Microphone() as source:
        r.adjust_for_ambient_noise(source, duration=1)
        print("Dites quelque chose!")
        audio = r.listen(source)
    # Speech recognition using Google Speech Recognition
    data = ""
    try:
        # Uses the default API key
        # To use another API key: `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
        data = r.recognize_google(audio, language='fr-FR')
    except sr.UnknownValueError:
        data = "La reconnaissance vocale Google ne comprenait pas l'audio"
    except sr.RequestError as e:
        data = "Could not request results from Google Speech Recognition service; {0}".format(e)

    return data


#######################################################
### bot d'accueil ###
#######################################################

def bot_home(msg):
    # Token du bot dialogflow
    CLIENT_ACCESS_TOKEN_HOME = '88360d86ac7049d5b10378ca5e695087'

    # recuperer l'intsntion et le reponse predifinie dans dialologflow
    datastore = ReponseBotDialog(msg, CLIENT_ACCESS_TOKEN_HOME)
    intent = datastore['result']['metadata']['intentName'];
    response = datastore['result']['fulfillment']['speech'];

    if intent == "connexionOM":
        return "botOM", response
    elif intent == "connexionRecla":
        return "botRECLA", response
    else:
        return "botHOME", response


#######################################################
### bot de orange money ###
#######################################################
def bot_om(msg):
    # Token du bot dialogflow
    CLIENT_ACCESS_TOKEN_OM = '7ce8abc895ff47a3a6d9317a8e75221c'

    # recuperer l'intsntion et le reponse predifinie dans dialologflow
    datastore = ReponseBotDialog(msg, CLIENT_ACCESS_TOKEN_OM)
    incomplet = str(datastore['result']['actionIncomplete'])
    intent = datastore['result']['metadata']['intentName']
    response = datastore['result']['fulfillment']['speech']

    if intent == "deconnexion":
        return "botHOME", False, "déconnexion éffectuée"
    elif intent == "Infos-client":
        if incomplet == "True":
            return "botOM", False, response
        else:
            Numero = datastore['result']['parameters']['number']
            print(selectClient(Numero))
            msisdn, nom, prenom, mr = selectClient(Numero)
            if nom == None:
                return "botOM", False, "Ce numéro n\'existe pas."
            else:
                form = formdeplafon(msisdn, nom, prenom, mr)
                return "botOM", True, form
    else:
        return "botOM", False, response


#######################################################
### bot reclamation ###
#######################################################
def bot_re(msg):
    # Token du bot dialogflow
    CLIENT_ACCESS_TOKEN_RE = 'eae4c6cbb94542818f66e9b07fd8eb4d'

    datastore = ReponseBotDialog(msg, CLIENT_ACCESS_TOKEN_RE)

    # recuperer l'intsntion et le reponse predifinie dans dialologflow
    intent = datastore['result']['metadata']['intentName']
    # response = datastore['result']['fulfillment']['speech']

    if intent == "deconnexion":
        return "botHOME", False, "Déconnexion éffectuée!"
    if intent == "EnregistreDerangement":
        return "botRECLA", True, "home/formulaire_Client_Derangement.html"
    else:
        return "botRECLA", False, "Vous êtes connectés à votre assistant virtuel  pour les reclamations"


#########################################
## Base de donnees
###########################
def selectClient(numero):
    # variable
    id = ''
    nom = None
    prenom = ''
    statut = ''
    msisdn = ''
    mr = ''
    # conn = pymysql.connect(host='localhost', port=3306, user='debian-sys-maint', passwd='WMYIaYNvbdpH5WVv',
    #                        db='ChatbotVocal')
    conn = pymysql.connect(host='localhost', user='root', passwd='root', db='ChatbotVocal')

    cursor = conn.cursor()

    sql = "SELECT * FROM clients where Msisdn like '%s'" % (numero)
    try:
        # Execute the SQL command
        cursor.execute(sql)
        # # Fetch all the rows in a list of lists.
        # results = cursor.fetchall()
        # prenom(cursor.description)
        for row in cursor:
            # id = row[0]
            msisdn = row[1]
            nom = row[2]
            prenom = row[3]
            # statut = row[4]
            mr = row[5]
    except:
        print("Error: unable to fetch data")
    # disconnect from server
    conn.close()
    return msisdn, nom, prenom, mr


###################################
# enregistrer un derangement
###################################
def enrigistrerDerangement(Ticket):
    # conn = pymysql.connect(host='localhost', port=3306, user='debian-sys-maint', passwd='WMYIaYNvbdpH5WVv',
    #                        db='ChatbotVocal')

    conn = pymysql.connect(host='localhost', user='root', passwd='root', db='ChatbotVocal')

    cursor = conn.cursor()
    id = 0
    sql1 = "INSERT INTO `tickets`(`id`, `nomClient`, `prenomClient`, `telephone`, `typeDerangement`, `commentaire`, `usernameAgent`, `date`) VALUES ('%d','%s','%s','%s','%s','%s','%s','%s')" % (
        id, Ticket.Nom, Ticket.Prenom, Ticket.Telephone, Ticket.TypeDerangement, Ticket.Commentaire, Ticket.Username,
        Ticket.Date
    )
    # sql = "SELECT * FROM clients where Msisdn like '%s'" % (numero)
    try:
        # Execute the SQL command
        cursor.execute(sql1)
        conn.commit()
    except:
        print("Error: unable to fetch data")


########################################
# #liste des enregistements selon l utilisateur
########################################
def listderangement(user):
    tickets = []
    # conn = pymysql.connect(host='localhost', port=3306, user='debian-sys-maint', passwd='WMYIaYNvbdpH5WVv',
    #                        db='ChatbotVocal')

    conn = pymysql.connect(host='localhost', user='root', passwd='root', db='ChatbotVocal')

    # conn = pymysql.connect(host='localhost', user='root', passwd='root', db='ChatbotVocal')

    cursor = conn.cursor()

    sql = "SELECT * FROM tickets where usernameAgent like '%s'" % (user)
    try:
        # Execute the SQL command
        cursor.execute(sql)
        # # Fetch all the rows in a list of lists.
        # results = cursor.fetchall()
        # prenom(cursor.description)
        for row in cursor:
            ticket = Ticket(row[1], row[2], row[3], row[4], row[5], row[6], row[7])
            # id = row[0]

            tickets.append(ticket)

    except:
        print("Error: unable to fetch data")
    # disconnect from server
    conn.close()
    return tickets


###############################
##formulaire motif de rejet de deplafonnement
##############################
def formdeplafon(msisdn, nom, prenom, mr):
    form = "<table class=table table-bordered>" \
           "<tr> " \
           "<th scope=col>Informations client</th>" \
           "<th scope=col></th>" \
           "</tr>" \
           "<tr> " \
           "<td> Prénom :</td> " \
           " <td><b>" + prenom + "</b></td>" \
                                 " </tr>" \
                                 " <tr> " \
                                 "<td> Nom :</td> " \
                                 "<td><b>" + nom + "</b></td> " \
                                                   "</tr> " \
                                                   "<tr> " \
                                                   "<td> Msisdn :</td>" \
                                                   "<td><b>" + msisdn + "</b></td>" \
                                                                        "</tr>" \
                                                                        "<tr>" \
                                                                        "<td>Motif de rejet :</td> " \
                                                                        "<td><b>" + mr + "</b></td>" \
                                                                                         "</tr> </table>"
    return form

# print(listderangement("Fallou"))
# print (selectClient("77 714 45 28"))
# ticks = Ticket("DIENG","Fallou","777144528","Orange Money", "test","Fallou","18/05/2018")
# enrigistrerDerangement(ticks)
