from flask import flash, url_for, redirect, render_template, request, current_app
from flask_login import login_user, login_required, logout_user

from app import db
from . import auth
from .forms import RegistrationForm, LoginForm
from ..models import Agent
from ..voice_recognition.RecordAndPredict import recordAudio


@auth.route('/register', methods=['GET', 'POST'])
def register():
    """
    Handle requests to the /register route
    Ajouter un utilisateur à la BD grace au formulaire
    """
    form = RegistrationForm()
    if form.validate_on_submit():
        agent = Agent(username=form.username.data,
                      password=form.password.data)

        # add employee to the database
        db.session.add(agent)
        db.session.commit()
        flash('Utilisateur Créé avec succés')

        # redirect to the login page
        return redirect(url_for('auth.index'))

    # load registration templates
    return render_template('auth/register.html', form=form, title='Register')


@auth.route('/', methods=['GET', 'POST'])
def login():
    typeAuthentication = request.args.get('typeAuth')
    form = LoginForm()

    if typeAuthentication is None:

        if form.validate_on_submit():
            user = Agent.query.filter_by(username=form.username.data).first()
            if user is not None and user.verifier_password(form.password.data):
                # Connexion de l'utilisateur
                login_user(user, remember=form.remember_me.data)
                # Initialisation d'un msg Flash
                flash('Bienvenue ' + str(user))
                # Redirection vers la page d'accueil
                return redirect(url_for('home.dashboard'))

            flash('Invalid username or password')
            return render_template('auth/login.html')

    if typeAuthentication == "vocal":
        speakerGMM, probaGMM, etat = recordAudio()

        print(speakerGMM, probaGMM, etat)
        if etat == "ok" and speakerGMM != "Autre":
            user = Agent.query.filter_by(username=speakerGMM.lower()).first()
            print(user)
            login_user(user)
            flash('Bienvenue ' + str(user))
            return "authentifie"
        else:
            return "echec-authentification"

    return render_template('auth/login.html', form=form)


@auth.route('/logout')
@login_required
def logout():
    """
    Deconnecter un utilisateur
    """
    logout_user()
    flash('Deconnexion bien effectuée')

    # redirect to the login page
    return redirect(url_for('auth.login'))



