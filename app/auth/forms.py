from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import DataRequired, EqualTo, ValidationError
from flask_wtf import FlaskForm

from ..models import Agent
from .. import db


class LoginForm(FlaskForm):
    username = StringField(id("inputUser"), validators=[DataRequired()])
    password = PasswordField(id("inputPwd"), validators=[DataRequired()])
    remember_me = BooleanField()

    # def validate_login(self, field):
    #     user = self.get_user()
    #
    #     if user is None:
    #         raise validators.ValidationError('Invalid user')
    #
    #     # we're comparing the plaintext pw with the the hash from the db
    #     if not user.verifier_password(field.password.data):
    #     # to compare plain text passwords use
    #     # if user.password != self.password.data:
    #         raise validators.ValidationError('Invalid password')

    def get_user(self):
        return db.session.query(Agent).filter_by(username=self.login.data).first()


class RegistrationForm(FlaskForm):
    """
    Form for users to create new account
    """
    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired(), EqualTo('confirm_password')])
    confirm_password = PasswordField('Confirmer Mot de Passe')

    @staticmethod
    def validate_username(field):
        if Agent.query.filter_by(username=field.data).first():
            raise ValidationError("Nom d'utilisateur déjà utilisé")
