from flask_jwt_extended import create_access_token, create_refresh_token, jwt_required
from flask_restful import Resource, reqparse

from app.botVocal import bot_home, bot_om, bot_re
from app.models import Agent

parser = reqparse.RequestParser()
parser.add_argument('username', help='Ne peut être vide', required=True)
parser.add_argument('password', help='Ne peut être vide', required=True)
parser.add_argument('is_admin')
parser.add_argument('msg')
parser.add_argument('bot')


class UserRegistration(Resource):
    @jwt_required
    def post(self):
        data = parser.parse_args()

        if Agent.find_by_username(data['username']):
            return {'message': 'User {} already exists'.format(data['username'])}

        new_user = Agent(
            username=data['username'],
            password_hash=Agent.generate_hash(data['password']),
            is_admin=data['is_admin']
        )

        try:
            new_user.save_to_db()
            access_token = create_access_token(identity=data['username'])
            refresh_token = create_refresh_token(identity=data['username'])
            return {
                'message': 'User {} was created'.format(data['username']),
                'access_token': access_token,
                'refresh_token': refresh_token
            }

        except:

            return {'message': 'User {} was created'.format(data['username'])}


class UserLogin(Resource):
    def post(self):
        data = parser.parse_args()
        current_user = Agent.find_by_username(data['username'])
        if not current_user:
            return {'message': 'User {} doesn\'t exist'.format(data['username'])}

        if current_user.verifier_password(data['password']):
            access_token = create_access_token(identity=data['username'])
            refresh_token = create_refresh_token(identity=data['username'])
            return {
                'message': 'Logged in as {}'.format(current_user.username),
                'access_token': access_token,
                'refresh_token': refresh_token
            }
        else:
            return {'message': 'Wrong credentials'}


class AllUsers(Resource):
    @jwt_required
    def get(self):
        return Agent.return_all()

    def delete(self):
        return Agent.delete_all()


class GetbotReponse(Resource):
    @jwt_required
    def post(self):
        data = parser.parse_args()
        if data['msg'] and data['bot']:

            if data['bot'] == "botHome":
                idbotG, reponse = bot_home(data['msg'])
            elif data['bot'] == "botOM":
                idbotG, isformOM, reponse = bot_om(data['msg'])
            elif data['bot'] == "botRecla":
                idbotG, isformRE, reponse = bot_re(data['msg'])
            else:
                return {'message': "Erreur!! Veuillez spécifier un idbot correct: botHome ou botOM ou botRecla"}

            return {'bot': idbotG, 'messageInput': data['msg'], 'reponseBot': reponse}

        return {'message': "Erreur!! veuillez entrer les paramètres d'entrée: idbot et msg"}
