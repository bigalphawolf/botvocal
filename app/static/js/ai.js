responsiveVoice.setDefaultVoice("French Female");


    function speak(text2speak) {

        if(responsiveVoice.voiceSupport() && text2speak.length < 300) {
            //Ce la permet d'activer la fonction speak sssi le texte n'est pas long
            responsiveVoice.speak(text2speak);
        }

    }

botui = new BotUI('main-interface');

// Au debut on est connecté à l'Assistant virtuel ou botHome
var idBot = "botHOME";

      messageBienvenue = "Bonjour et bienvenue dans l'assistant virtuel de la Sonatel.";

      speak(messageBienvenue);

      botui.message.add({
          content: messageBienvenue
      });


      //Fonction récupération entrée txt et affichage dans BOT UI
      function showTextMsg(msg) {

          botui.message.add({
              // delay: 1000,
              human: true,
              content: msg
          });


      }

      function reponseBot(msgUser, bot) {
          $.get("/get", { msg: msgUser, bot: bot }).done(function(data)
          {
              if(data.bot){
                  // Initialiser la variable bot pour
                  idBot = data.bot;
                  // alert(idBot);

                  //  Réponse Audio du bot
                  msg = data.reponse;

                  speak(msg);

                  //Réponse Textuelle du bot
                  botui.message.add({
                      delay: 700,
                      loading: true,
                      content: msg
                  });

                  //  Affichage du nom du bot auquel on est connecté
                  showConnectedBot(idBot);

              }else{
                  //Réponse Audio du bot
                  speak(data);

                  //Réponse Textuelle du bot
                  botui.message.add({
                      delay: 700,
                      loading: true,
                      content: data
                  });
              }


          });
          $("#inputMsg").val("");
      }

function showConnectedBot(bot) {
    switch (bot){
        case "botHOME":
            $("#AssistantBot").html("<span class=\"badge badge-pill orange\">Connecté</span>");
            $("#OMBot").html("");
            $("#ReclamBot").html("");
            break;
        case "botOM":
            $("#AssistantBot").html("");
            $("#OMBot").html("<span class=\"badge badge-pill orange\">Connecté</span>");
            $("#ReclamBot").html("");
            break;
        default:
            $("#AssistantBot").html("");
            $("#OMBot").html("");
            $("#ReclamBot").html("<span class=\"badge badge-pill orange\">Connecté</span>");
            break;
    }
}




      function sendAudioMsg() {
            speak("Je vous écoute");
            botui.message.add({
                content: "Je vous écoute"
            });


            $.get("/getVocal").done(function(data) {
                data1 = data.split(";");

                //Transcription de la requete vocale de l'utilisateur en texte
                botui.message.add({
                    delay: 2000,
                    human: true,
                    content: data1[0]
                });

                //Réponse Audio du bot
                speak(data1[1]);

                //Réponse Textuelle du bot

                botui.message.add({
                    delay: 2000,
                    loading: true,
                    content: data1[1]
                });

            });
      }

      function submitMsg(message,chatbotConnecte) {

          $.get("/get",{msg: message, bot:chatbotConnecte}).done(function(data) {
            //Transcription de la requete vocale de l'utilisateur en texte
                botui.message.add({
                    delay: 2000,
                    human: true,
                    content: message
                });

                //Réponse Audio du bot
                speak(data1[1]);

                //Réponse Textuelle du bot

                botui.message.add({
                    delay: 2000,
                    loading: true,
                    content: data
                });

            });

      }

      //Speak to text
      function Audio2Text() {

            if(window.hasOwnProperty('webkitSpeechRecognition')) {

                speak("Je vous écoute");
                    botui.message.add({
                    content: "Je vous écoute"
                });

                var recognition = new webkitSpeechRecognition();
                 recognition.continuous  = false;
                 recognition.interimResults = false;
                 recognition.lang = "fr-FR";
                 recognition.start();

                 recognition.onresult = function(e) {
                     var message =  e.results[0][0].transcript;
                     showTextMsg(message);
                     reponseBot(message,idBot);
                     // alert(message);
                     recognition.stop();

                 };

                  recognition.onerror = function(e) {
                       botui.message.add({
                            delay: 2000,
                            loading: true,
                            content: 'erreur'});
                      recognition.stop();
                    }
            }else {
                alert("Veuillez svp utilise un navigateur Chrome");
            }
       }


      //Envoyer msg s'il appuie sur la touche Entrée
      $("#inputMsg").keypress(function(e){
            if (e.which == 13) {
              msgTxt = $("#inputMsg").val();

              showTextMsg(msgTxt);
              reponseBot(msgTxt,idBot);

            }
      });

      //Envoyer msg s'il appuie sur le bouton Envoyer
      $("#boutonSendText").click(function () {
            msgTxt = $("#inputMsg").val();

            showTextMsg(msgTxt);
            reponseBot(msgTxt,idBot);
      });

      //Appeler sendAudioMsg lorsque l'on appuie sur le bouton record
      $("#boutonSendAudio").click(function () {
           Audio2Text();

      });



