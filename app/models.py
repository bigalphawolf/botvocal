from app import db, login_manager
from flask_login import UserMixin
from passlib.hash import pbkdf2_sha256 as sha256
from werkzeug.security import check_password_hash, generate_password_hash


class Agent(UserMixin, db.Model):
    """ Création de la classe Agent """

    # Nom de la table dans la BD
    __tablename__ = 'agents'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True)
    password_hash = db.Column(db.String(128))
    is_admin = db.Column(db.Boolean, default=False)
    tickets = db.relationship('Ticket', backref='Agent',
                              lazy='dynamic')

    # def __init__(self, username, pwd):
    #     self.username = username
    #     self.password = pwd

    def __repr__(self):
        return self.username

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'username': x.username,
                'password': x.password_hash
            }

        return {'users': list(map(lambda x: to_json(x), Agent.query.all()))}

    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} Ligne(s) Supprimée(s)'.format(num_rows_deleted)}
        except:
            return {'message': 'Une erreur est survenu'}

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    def verifier_password(self, password):
        """
        Check if hashed password matches actual password
        """
        return check_password_hash(self.password_hash, password)

    @staticmethod
    def check_password(pwd_hashe, password):
        """
        Check if hashed password matches actual password
        """
        return check_password_hash(pwd_hashe, password)

    @staticmethod
    def generate_hash(password):
        return generate_password_hash(password)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()


@login_manager.user_loader
def load_agent(user_id):
    return Agent.query.get(int(user_id))


class Ticket(db.Model):
    # Nom de la table dans la bd
    __tablename__ = 'tickets'

    id = db.Column(db.Integer, primary_key=True)
    nomClient = db.Column(db.String(60))
    prenomClient = db.Column(db.String(80))
    telephone = db.Column(db.String(30))
    typeDerangement = db.Column(db.String(60))
    commentaire = db.Column(db.String(200))
    date = db.Column(db.String(20))
    agent_id = db.Column(db.Integer, db.ForeignKey('agents.id'))

    def __init__(self, Nom, Prenom, Telephone, TypeDerangement, Commentaire, Date, agent_id):
        self.nomClient = Nom
        self.prenomClient = Prenom
        self.telephone = Telephone
        self.typeDerangement = TypeDerangement
        self.commentaire = Commentaire
        self.date = Date
        self.agent_id = agent_id

    def __repr__(self):
        return self.nomClient

    def enregistrer(self):
        db.session.add(self)
        db.session.commit()


class RejetOM(db.Model):
    # Nom de la table dans la BD
    __tablename__ = 'clientsOM'

    id = db.Column(db.Integer, primary_key=True)
    prenom = db.Column(db.String(80))
    nom = db.Column(db.String(60))
    msisdn = db.Column(db.String(20))
    motifRejet = db.Column(db.String(400))

    def __repr__(self):
        return self.prenom, self.nom, self.msisdn, self.motifRejet
