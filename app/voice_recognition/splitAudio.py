#/home/deeplearning/PycharmProjects/voice-classification-master
import subprocess
import os
from voice_recognition.silence import silence_mono

def Stereo2Mono(rootdir,outdir):
    subdir = os.listdir(rootdir)

    for sub in subdir:
        count = 0
        output_dir = outdir + sub
        intermediate = './intermediate/' + sub
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
            os.makedirs(intermediate)
        files1 = os.path.join(rootdir,sub)
        files = os.listdir(files1)
        count = 0
        for file in files:
            in_path = files1 + '/' + file
            smdstring1 = 'sox "{}" "{}" remix 2'.format(in_path, intermediate+'/out00{}.r.wav'.format(count))
            smdstring2 = 'sox "{}" "{}" remix 1'.format(in_path, intermediate+ '/out00{}.l.wav'.format(count))
            smdstring3 = 'sox -m "{}" "{}" "{}"'.format(intermediate+'/out00{}.l.wav'.format(count), intermediate+ '/out00{}.r.wav'.format(count),output_dir + '/out00{}.mono.wav'.format(count))
            subprocess.call(smdstring1,shell=True)
            subprocess.call(smdstring2,shell=True)
            subprocess.call(smdstring3,shell=True)
            count+=1


def splitAudio(rootdir,outdir ):
    subdir = os.listdir(rootdir)
    for sub in subdir:
        count = 0
        output_dir = outdir + sub
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        files1 = os.path.join(rootdir,sub)
        files = os.listdir(files1)
        for file in files:
            in_path = files1 + '/' + file
            silence_mono(in_path)
            out_path = output_dir+'/'+'{}out%03d.wav'.format(count)
            subprocess.call(['ffmpeg', '-i', in_path, '-f', 'segment',
                               '-segment_time', '5', '-c', 'copy', out_path])
            count += 1


# Wav2Spet

def wav2spect(rootdir,outdir ):
     for subdir, dirs, files in os.walk(rootdir):
         output_dir = outdir + os.path.basename(subdir)
         if not os.path.exists(output_dir):
             os.makedirs(output_dir)
         count = 0
         for file in files:
            in_path = subdir + '/' + file
            cmdstring = 'sox "{}" -n spectrogram -r -o "{}"'.format(in_path, output_dir+'/out00{}.png'.format(count))
            subprocess.call(cmdstring, shell=True)
            count +=1



# splitAudio('./audios/','./audiosplit/')
# wav2spect('./audiosplit/','./spects/')
#Stereo2Mono('./audiosplit/','./monowav/')