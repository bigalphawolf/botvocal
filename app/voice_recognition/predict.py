import keras
from keras.models import Model

from keras.applications import VGG16

from keras.layers import Dense, Dropout, Flatten

from keras.preprocessing.image import load_img, img_to_array
import numpy as np
import subprocess

img_size = 224
base_model = VGG16(include_top=False, weights=None, input_shape=(img_size, img_size, 3))

x = base_model.output
x = Flatten()(x)
x = Dense(1024, activation="relu")(x)
x = Dropout(0.5)(x)
classes = 2
classeName = ["Autre", "Fallou", "Karim"]


def predict(imageFile):
    # imageFile= "carte-validation/carte-guinee/8495324.jpg"
    predictions = Dense(classes, activation='softmax')(x)

    model = Model(inputs=base_model.input, outputs=predictions)

    model.load_weights("cv-tricks_fine_tuned_model3.h5")

    inputShape = (224, 224)  # Assumes 3 channel image
    image = load_img(imageFile, target_size=inputShape)
    image = img_to_array(image)  # shape is (224,224,3)
    image = np.expand_dims(image, axis=0)  # Now shape is (1,224,224,3)

    image = image / 255.0

    speaker = model.predict(image)
    # proba = np.max(model.predict_proba(image))
    print(speaker)

# cmdstring = 'sox "{}" -n spectrogram -r -o "{}"'.format("./wavAuthentification/fallou8.wav","voiceAuthentification/fallou8.png")
# subprocess.call(cmdstring, shell=True)
# predict("/home/deeplearning/PycharmProjects/botvocalf/voice_recognition/voiceAuthentification/fallou8.png")
