import librosa
import numpy as np


def silence_mono(filepath):
    y, sr = librosa.load(filepath)
    # Trim the beginning and ending silence
    yt, index = librosa.effects.trim(y, 10)

    # print("Durée sans silence{}".format(librosa.get_duration(yt)) + "\n Durée avec silence{}".format(
    #     librosa.get_duration(y)))
    y_mono = librosa.to_mono(yt)
    librosa.output.write_wav(filepath, y_mono, sr)
