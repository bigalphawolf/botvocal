import os
import _pickle as cPickle
import numpy as np
from scipy.io.wavfile import read
import warnings

from config import BaseConfig
from voice_recognition.GMMMethode.Feature_Extract import extract_features

warnings.filterwarnings("ignore")
import time


def QuiParle(audioPath):
    speaker = "unknow"
    # path to training data
    # source = "./test2/Karim/"
    # source = "./test/Fallou-1out004.wav"
    modelpath = BaseConfig.MODELS_PATH_AK
    # test_file = "set_test.txt"
    # file_paths = open(test_file, 'r')

    gmm_files = [os.path.join(modelpath, fname) for fname in
                 os.listdir(modelpath) if fname.endswith('.gmm')]
    # print(gmm_files)
    # Load the Gaussian gender Models

    models = [cPickle.load(open(fname, 'rb')) for fname in gmm_files]

    speakers = [fname.split("/")[-1].split(".gmm")[0] for fname in gmm_files]

    # print(str(speakers))

    # Read the test directory and get the list of test audio files
    # for path in file_paths:
    #     path = path.strip()

    # print(path)
    sr, audio = read(audioPath)
    vector = extract_features(audio, sr)

    log_likelihood = np.zeros(len(models))
    # print(str(np.exp(log_likelihood[0])) + "---" + str(np.exp(log_likelihood[1])))

    for i in range(len(models)):
        # print(len(models))
        gmm = models[i]  # checking with each model one by one
        # print(gmm)
        scores = np.array(gmm.score(vector))

        log_likelihood[i] = scores.sum()

    winner = np.argmax(log_likelihood)
    somme = -1 * log_likelihood.sum()

    proba = (log_likelihood[winner] + somme) / somme
    if proba > 0.51:
        speaker = speakers[winner]

    return speaker,proba
    # time.sleep(1.0)


# print(QuiParle("/home/bigbadwolf/Documents/botvocal/voice_recognition/voiceAuthentification/record.wav"))
