import os

import keras
import numpy as np
from tensorflow.python.keras._impl.keras.utils import to_categorical

from instance.config import LABELS_PATH_CNN, MODELS_PATH_CNN
from ..GMMMethode.preprocess import wav2mfcc


feature_dim_2 = 11
feature_dim_1 = 20
channel = 1


modelspath = MODELS_PATH_CNN
labels = LABELS_PATH_CNN

# def predictspeaker(filepath):
#     modelf = keras.models.load_model('/home/deeplearning/PycharmProjects/botvocalf/voice_recognition/GMMMethode/Fallou')
#     modelk = keras.models.load_model(
#         '/home/deeplearning/PycharmProjects/botvocalf/voice_recognition/GMMMethode/Karim')
#     speaker = 'Autre'
#     sample = wav2mfcc(filepath)
#     sample_reshaped = sample.reshape(1, feature_dim_1, feature_dim_2, channel)
#     probaf = np.max(modelf.predict_proba(sample_reshaped))
#     probak = np.max(modelk.predict_proba(sample_reshaped))
#
#     speakerf = get_labels()[0][
#                 np.argmax(modelf.predict(sample_reshaped))
#         ]
#     speakerk = get_labels()[0][
#                 np.argmax(modelk.predict(sample_reshaped))
#         ]
#
#     return speakerf,probaf,speakerk,probak


def getlabels(path):
    labels = os.listdir(path)
    label_indices = np.arange(0, len(labels))
    return labels, label_indices, to_categorical(label_indices)


def predictspeakers(filepath):

   speaker = 'Autre'
   proba = 0.91
   speakers = {"Autre":-1, "Fallou": 0, "Karim":0}

   sample = wav2mfcc(filepath)
   sample_reshaped = sample.reshape(1, feature_dim_1, feature_dim_2, channel)

   models = os.listdir(modelspath)
   for model in models:
     modelpath = modelspath+model
     labelpath = labels+model
     os.chdir(labelpath)
     modelpkl = keras.models.load_model(modelpath)
     speakerpredict = getlabels(labelpath)[0][
                np.argmax(modelpkl.predict(sample_reshaped))
        ]
     speakers[speakerpredict]+= 1

   if sum(speakers.values()) ==1:
     for spk, val in speakers.items():
          if val == 1 :
              speaker =spk
   return speaker, proba