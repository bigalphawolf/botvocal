import argparse

from datetime import time
from time import time

from time import time
import keras
from keras.models import Model

from keras.applications import VGG16
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras.layers import GlobalAveragePooling2D, Dense, Dropout, Flatten

# from dataset import read_train_sets

img_size=224
ap = argparse.ArgumentParser()
train_dir =  "./train"
validation_dir = "./test"
classes = 2
batch_size=10

train_datagen = keras.preprocessing.image.ImageDataGenerator(
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)

test_datagen = keras.preprocessing.image.ImageDataGenerator(rescale=1. / 255)
train_generator = train_datagen.flow_from_directory(
        train_dir,
        target_size=(img_size, img_size),
        batch_size=batch_size,
        class_mode='categorical')

validation_generator = test_datagen.flow_from_directory(
        validation_dir,
        target_size=(img_size,img_size),
        batch_size=batch_size,
        class_mode='categorical')
base_model = VGG16(weights = "imagenet", include_top=False, input_shape = (img_size, img_size, 3))



i = 0
for layer in base_model.layers:
        layer.trainable = False
        i = i + 1
        print(i, layer.name)

x = base_model.output
x = Flatten()(x)
x = Dense(1024, activation="relu")(x)
x = Dropout(0.5)(x)
# x = Dense(128, activation='sigmoid')(x)
# x = GlobalAveragePooling2D()(x)
# x = Dropout(0.2)(x)
predictions = Dense(classes, activation='softmax')(x)

tensorboard = TensorBoard(log_dir="logs/{}".format(time()))

# filepath = 'cv-tricks_pretrained_model2.h5'
# checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=False,
#                              mode='auto', period=1)

filepath = 'cv-tricks_pretrained_model3.h5'
checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=False,
                             mode='auto', period=1)
callbacks_list = [checkpoint, tensorboard]

model = Model(inputs=base_model.input, outputs=predictions)

model.compile(loss="categorical_crossentropy", optimizer=keras.optimizers.SGD(lr=0.01, momentum=0.9), metrics=["accuracy"])

model.fit_generator(
        train_generator,
        steps_per_epoch=100,
        epochs=10,
        callbacks=callbacks_list,
        validation_data=validation_generator,
        validation_steps=20
)