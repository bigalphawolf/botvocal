
# We pass weights=None since we don't want any weights here
from time import time
import keras
from keras.models import Model

from keras.applications import VGG16
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras.layers import GlobalAveragePooling2D, Dense, Dropout, Flatten

img_size=224
train_dir = "./train"
validation_dir = "./test"
classes = 2


batch_size=10

train_datagen = keras.preprocessing.image.ImageDataGenerator(
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)

test_datagen = keras.preprocessing.image.ImageDataGenerator(rescale=1. / 255)
train_generator = train_datagen.flow_from_directory(
        train_dir,
        target_size=(img_size, img_size),
        batch_size=batch_size,
        class_mode='categorical')

validation_generator = test_datagen.flow_from_directory(
        validation_dir,
        target_size=(img_size,img_size),
        batch_size=batch_size,
        class_mode='categorical')

base_model = VGG16(include_top=False, weights=None, input_shape = (img_size, img_size, 3))
tensorboard = TensorBoard(log_dir="logs/{}".format(time()))

x = base_model.output

x = base_model.output
x = Flatten()(x)
x = Dense(1024, activation="relu")(x)
x = Dropout(0.5)(x)
predictions = Dense(classes, activation='softmax')(x)
filepath = 'cv-tricks_fine_tuned_model3.h5'
checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=False,
                             mode='auto', period=1)
callbacks_list = [checkpoint, tensorboard]

model = Model(inputs=base_model.input, outputs=predictions)

# After creating the network
model.load_weights("./cv-tricks_pretrained_model3.h5")


model.compile(loss="categorical_crossentropy", optimizer=keras.optimizers.SGD(lr=0.001, momentum=0.9),metrics=["accuracy"])
model.fit_generator(
        train_generator,
        steps_per_epoch=100,
        epochs=10,
        callbacks=callbacks_list,
        validation_data=validation_generator,
        validation_steps=20
)