import speech_recognition as sr

from instance.config import RECORD_PATH, RECORD_SPECT_PATH
from ..voice_recognition.GMMMethode.predictCNN import predictspeakers
from ..voice_recognition.silence import silence_mono
from config import BaseConfig

pathWav = RECORD_PATH
pathSpect = RECORD_SPECT_PATH


def recordAudio():
    speaker = 'Autre'
    proba = 0
    # La variable etat permet de savoir si la reconnaissance s'est bien passée
    # etat = ok si elle c'est bien passé, non-ok sinon
    etat = 'ok'

    # Record Audio
    r = sr.Recognizer()
    with sr.Microphone() as source:
        r.dynamic_energy_adjustment_ratio = 1.5
        r.adjust_for_ambient_noise(source, duration=0.5)
        print('enoncer la phrase : je veux me connecter a l assistant virtuel')
        audio = r.record(source, duration=4)
        print('stop')
    try:
        data = r.recognize_google(audio, language='fr-FR')
        print(data)
        if "je veux me connecter" in data:
            with open(pathWav, "wb") as f:
                f.write(audio.get_wav_data())
            # supprimer le silience et convertir en mono
            silence_mono(pathWav)
            # predict with cnn model
            speaker, proba = predictspeakers(pathWav)
            # speaker, proba, etat = QuiParle(path)

            # cmdstring = 'sox "{}" -n spectrogram -r -o "{}"'.format(pathWav, pathSpect)
            # subprocess.call(cmdstring, shell=True)

    except:
        return 'Autre', proba, "non-ok"

    return speaker, proba, etat


def recordSend():
    r = sr.Recognizer()
    with sr.Microphone() as source:
        r.adjust_for_ambient_noise(source, duration=1)
        print("Dites quelque chose!")
        audio = r.listen(source)
    # Speech recognition using Google Speech Recognition
    data = ""
    try:
        # Uses the default API key
        # To use another API key: `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
        data = r.recognize_google(audio, language='fr-FR')
    except sr.UnknownValueError:
        data = "La reconnaissance vocale Google ne comprenait pas l'audio"
    except sr.RequestError as e:
        data = "Could not request results from Google Speech Recognition service; {0}".format(e)

    return data

# print(recordAudio())
