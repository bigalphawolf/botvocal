import keras
from keras.layers import Conv2D, MaxPooling2D
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
import pickle
import numpy as np
from sklearn.preprocessing import LabelEncoder
from keras.utils import np_utils
from keras.models import Model
from sklearn.metrics import accuracy_score

from sklearn import svm

model = Sequential()
model.add(Conv2D(8, (3, 3), padding='same',
                 input_shape=(513, 800, 3)))
model.add(Activation('relu'))
model.add(Conv2D(8, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(16, (3, 3), padding='same'))
model.add(Activation('relu'))
model.add(Conv2D(16, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
# model.add(Dropout(0.25))

model.add(Flatten())
# model.add(Dense(10))
model.add(Activation('relu'))
# model.add(Dropout(0.5))
model.add(Dense(57))
model.add(Activation('softmax'))

model.summary()

# initiate RMSprop optimizer
opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)

# Let's train the model using RMSprop
model.compile(loss='categorical_crossentropy',
               optimizer=opt,
              metrics=['accuracy'])

model2 = Model(inputs=model.input, outputs=model.get_layer('flatten_1').output)
model.load_weights('my_model_weights.h5')

clf = svm.SVC(kernel='rbf', class_weight='balanced')
clf.probability = True

import os
import matplotlib.pyplot as plt


def train():
    rootdir = './train/'

    spectograms = []
    spect_read = []
    spectograms_ids = []
    for subdir, dirs, files in os.walk(rootdir):
        for file in files:
            if file.endswith('png'):
                try:
                    x = plt.imread(subdir + '/' + file)
                except:
                    continue
                if str(x.shape) == '(513, 800, 3)':
                    spect_read.append(x)
                    # print(subdir)
                    name = subdir.replace(rootdir, '')
                    # print(name)
                    # name = name.replace('/spects', "")
                    spectograms_ids.append(name)
                    spectograms.append(file)
    x_train = spect_read
    y_train = spectograms_ids

    encoder = LabelEncoder()
    y_temp_train = y_train
    encoder.fit(y_temp_train)
    encoded_Y = encoder.transform(y_temp_train)
    dummy_y = np_utils.to_categorical(encoded_Y)

    svm_x_train = []
    svm_y_train = []
    for i in range(len(x_train)):
        x_1 = np.expand_dims(x_train[i], axis=0)
        flatten_2_features = model2.predict(x_1)
        svm_x_train.append(flatten_2_features)
        svm_y_train.append(dummy_y[i])

    svm_x_train = np.array(svm_x_train)
    clf = svm.SVC(kernel='rbf', class_weight='balanced', probability=True)
    dataset_size = len(svm_x_train)
    svm_x_train = np.array(svm_x_train).reshape(dataset_size, -1)
    svm_y_train = np.array(svm_y_train)
    svm_y_train = [np.where(r == 1)[0][0] for r in svm_y_train]

    clf.fit(svm_x_train, svm_y_train)
    pickle.dump(clf,open('model.pkl','wb'))
    print('model trained and saved')



def test():
    clf = pickle.load(open('model.pkl', 'rb'))
    rootdir = './test/'
    spectograms = []
    spect_read = []
    spectograms_ids = []
    for subdir, dirs, files in os.walk(rootdir):
        for file in files:
            if file.endswith('png'):
                try:
                    x = plt.imread(subdir + '/' + file)
                except:
                    continue
                if str(x.shape) == '(513, 800, 3)':

                    spect_read.append(x)
                    name = subdir.replace(rootdir, '')
                    # name = name.replace('/spects', "")
                    spectograms.append(file)
                    spectograms_ids.append(name)
    x_test = spect_read
    y_test = spectograms_ids

    y_temp2_train = y_test
    encoder = LabelEncoder()
    encoder.fit(y_temp2_train)
    encoded_Y = encoder.transform(y_temp2_train)
    dummy2_y = np_utils.to_categorical(encoded_Y)
    svm_x_test = []
    svm_y_test = []
    for i in range(len(x_test)):
        x_1 = np.expand_dims(x_test[i], axis=0)
        # x_1 = preprocess_input(x_1)
        flatten_2_features = model2.predict(x_1)
        svm_x_test.append(flatten_2_features)
        svm_y_test.append(dummy2_y[i])
    svm_x_test = np.array(svm_x_test)

    dataset_size = len(svm_x_test)
    svm_y_test = [np.where(r == 1)[0][0] for r in svm_y_test]
    svm_x_test = np.array(svm_x_test).reshape(dataset_size, -1)
    predictions = clf.predict_proba(svm_x_test)
    print(predictions)

    print(clf.predict(svm_x_test), '\n',
          accuracy_score(svm_y_test, clf.predict(svm_x_test)))


def predict(input_path):
    #load model
    clf  = pickle.load(open('model.pkl', 'rb'))
    classes = ['Autre','Fallou','Karim']
    best_class = 'unknow'
    x_predit = []
    x = plt.imread(input_path)
    str(x.shape) == '(513, 800, 3)'
    x_1 = np.expand_dims(x, axis=0)
    # x_1 = preprocess_input(x_1)
    flatten_2_features = model2.predict(x_1)
    x_predit.append(flatten_2_features)
    x_predit = np.array(x_predit)
    dataSize = len(x_predit)
    x_predit = np.array(x_predit).reshape(dataSize, -1)
    predictions = clf.predict_proba(x_predit)
    best_proba = np.argmax(predictions, axis=1)
    best_class_probabilities = predictions[np.arange(len(best_proba)), best_proba]
    # if best_class_probabilities[0] >= 0 and classes[clf.predict(x_predit)[0]]=='Fallou':
    #       best_class = classes[clf.predict(x_predit)[0]]
    # elif best_class_probabilities[0] >= 0 and classes[clf.predict(x_predit)[0]]=='Karim':
    #       best_class = classes[clf.predict(x_predit)[0]]
    print(clf.predict(x_predit)[0],best_class_probabilities[0])

# if __name__ == '__main__':
#                         train()
#                         test()
                  #                yousef = './inconnu/yousef/'
                  #                chris =  './inconnu/chris/'
                  #                karim = './train/Karim/'
                  #                 fallou = './train/Fallou/'
                  #                 imgs = os.listdir(fallou)
                  #                 for img in imgs:
                  #                     predict(fallou+img)
                  # # #   #               print(fallou+img)

